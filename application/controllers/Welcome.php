<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$em = $this->doctrine->em;
//		$this->create_update_database($em,$mode="update");
		$this->Publication_model->add_publication();

//		phpinfo();

		$this->load->view('welcome_message');
	}

	function create_update_database($em,$mode="update"){

		$tool = new \Doctrine\ORM\Tools\SchemaTool($em);

		$cmf = new \Doctrine\ORM\Tools\DisconnectedClassMetadataFactory();
		$cmf->setEntityManager($em);
		$metadata = $cmf->getAllMetadata();

		if($mode == "create"){
			$queries = $tool->getCreateSchemaSql($metadata);
		}
		else{
			$queries = $tool->getUpdateSchemaSql($metadata);
		}
		echo "Total queries: ".count($queries)."<br /><br />";
		for($i=0; $i<count($queries);$i++){
			$em->getConnection()->prepare($queries[$i])->execute();
			echo $queries[$i]."<br /><br />Execution Successful: ".($i+1)."<br /><br />";
		}
	}
}
