<?php
require_once(APPPATH."models/Entities/BookShop.php");
use \Entities\BookShop;
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 7:53 PM
 */
class BookShop_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_bookShop()
    {
        /**
         * @var BookShop $bookShop
         */
        $bookShop = new BookShop();
        $bookShop->setShopName('Sarasavi Bookshop');

        try {
            //save to database
            $this->em->persist($bookShop);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }
}