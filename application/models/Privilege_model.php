<?php

/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 1:46 PM
 */

require_once(APPPATH."models/Entities/Privilege.php");
use \Entities\Privilege;

class Privilege_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
    parent::__construct();
    $this->em = $this->doctrine->em;
}

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_privilege()
    {
        /**
         * @var Privilege $privilege
         */
        $privilege = new Privilege();
        $privilege->setPrivilegeName('CREATE_BOOK');
        $privilege->setPrivilegeLabel('Create Book');

        try {
            //save to database
            $this->em->persist($privilege);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }

}