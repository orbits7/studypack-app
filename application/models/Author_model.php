<?php

require_once(APPPATH."models/Entities/Author.php");
use \Entities\Author;
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 3:02 PM
 */
class Author_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_author()
    {
        /**
         * @var Author $author
         */
        $author = new Author();
        $author->setAuthorName('Waruna Udayanga');
        $author->setEmail('waruna@gmail.com');
        $author->setMobile('0123456789');
        $author->setNicNumber('563738222262V');

        try {
            //save to database
            $this->em->persist($author);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }


}