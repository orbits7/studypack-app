<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:46 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_subject")
 **/
class Subject
{
    /**
 * @Id @Column(type="integer")
 * @GeneratedValue
 **/
    protected $id;

    /**
     * @Column(name="subject_name", type="string", nullable=true)
     */
    private $subjectName;

    /**
     * @ManyToMany(targetEntity="Entities\SubjectCategory")
     * @JoinTable(
     *      name="subjects_categories",
     *      joinColumns={@JoinColumn(name="subject_id",onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="category_id",onDelete="CASCADE")})
     */
    private $subjectCategories;


    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->subjectCategories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSubjectName()
    {
        return $this->subjectName;
    }

    /**
     * @param mixed $subjectName
     */
    public function setSubjectName($subjectName)
    {
        $this->subjectName = $subjectName;
    }

    /**
     * @return mixed
     */
    public function getSubjectCategories()
    {
        return $this->subjectCategories;
    }

    /**
     * @param mixed $subjectCategories
     */
    public function setSubjectCategories($subjectCategories)
    {
        $this->subjectCategories = $subjectCategories;
    }
}