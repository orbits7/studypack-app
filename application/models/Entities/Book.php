<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:44 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_book")
 **/
class Book
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @ManyToOne(targetEntity="Entities\Subject")
     * @JoinColumn(name="subject_id", referencedColumnName="id")
     **/
    private $subject;

    /**
     * @ManyToOne(targetEntity="Entities\Author")
     * @JoinColumn(name="author_id", referencedColumnName="id")
     **/
    private $author;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

}