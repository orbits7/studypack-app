<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:43 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_user")
 **/
class User
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="title", type="string", nullable=true)
     */
    private $title;

    /**
     * @Column(name="full_name", type="string", nullable=true)
     */
    private $fullName;

    /**
     * @Column(name="username", type="string", nullable=true)
     */
    private $username;

    /**
     * @Column(name="password", type="string", nullable=true)
     */
    private $password;

    /**
     * @ManyToMany(targetEntity="\Entities\Role", inversedBy="users", cascade={"detach"})
     * @JoinTable(
     *      name="users_roles",
     *      joinColumns={@JoinColumn(name="user_id",onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="role_id",onDelete="CASCADE")})
     */
    private $roles;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param mixed $fullName
     */
    public function setFullName($fullName)
    {
        $this->fullName = $fullName;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add role
     *
     * @param \Entities\Role $role
     * @return User
     */
    public function addRole(Role $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \Entities\Role $role
     */
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

}