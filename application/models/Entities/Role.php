<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:43 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_role")
 **/
class Role
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="role_name", type="string", nullable=true)
     */
    private $roleName;

    /**
     * @Column(name="role_label", type="string", nullable=true)
     */
    private $roleLabel;

    /**
     * @ManyToMany(targetEntity="\Entities\Privilege", cascade={"detach"})
     * @JoinTable(
     *      name="roles_privileges",
     *      joinColumns={@JoinColumn(name="role_id",onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="privilege_id",onDelete="CASCADE")})
     */
    private $privileges;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->privileges = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRoleName()
    {
        return $this->roleName;
    }

    /**
     * @param mixed $roleName
     */
    public function setRoleName($roleName)
    {
        $this->roleName = $roleName;
    }

    /**
     * @return mixed
     */
    public function getRoleLabel()
    {
        return $this->roleLabel;
    }

    /**
     * @param mixed $roleLabel
     */
    public function setRoleLabel($roleLabel)
    {
        $this->roleLabel = $roleLabel;
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }


    /**
     * Add privilege
     *
     * @param \Entities\Privilege $role
     * @return Role
     */
    public function addPrivilege(Privilege $privilege)
    {
        $this->privileges[] = $privilege;

        return $this;
    }

    /**
     * Remove privilege
     *
     * @param \Entities\Privilege $privilege
     */
    public function removePrivilege(Privilege $privilege)
    {
        $this->privileges->removeElement($privilege);
    }

    /**
     * @param mixed $privileges
     */
    public function setPrivileges($privileges)
    {
        $this->privileges = $privileges;
    }
}