<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:44 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_privilege")
 **/
class Privilege
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="privilege_name", type="string", nullable=true)
     */
    private $privilegeName;

    /**
     * @Column(name="privilege_label", type="string", nullable=true)
     */
    private $privilegeLabel;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPrivilegeName()
    {
        return $this->privilegeName;
    }

    /**
     * @param mixed $privilegeName
     */
    public function setPrivilegeName($privilegeName)
    {
        $this->privilegeName = $privilegeName;
    }

    /**
     * @return mixed
     */
    public function getPrivilegeLabel()
    {
        return $this->privilegeLabel;
    }

    /**
     * @param mixed $privilegeLabel
     */
    public function setPrivilegeLabel($privilegeLabel)
    {
        $this->privilegeLabel = $privilegeLabel;
    }

}