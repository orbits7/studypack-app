<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:48 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_book_code")
 **/
class BookCode
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="publication_code", type="integer", nullable=true)
     */
    private $publicationCode;

    /**
     * @Column(name="is_cancelled", type="boolean", nullable=true)
     */
    private $isCancelled;

    /**
     * @ManyToOne(targetEntity="Entities\Book")
     * @JoinColumn(name="book_id", referencedColumnName="id")
     **/
    private $book;

    /**
     * @ManyToOne(targetEntity="Entities\PurchaseOrder", inversedBy="bookCodes")
     * @JoinColumn(name="purchase_order_id", referencedColumnName="id")
     **/
    private $purchaseOrder;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getPublicationCode()
    {
        return $this->publicationCode;
    }

    /**
     * @param mixed $publicationCode
     */
    public function setPublicationCode($publicationCode)
    {
        $this->publicationCode = $publicationCode;
    }

    /**
     * @return mixed
     */
    public function getIsCancelled()
    {
        return $this->isCancelled;
    }

    /**
     * @param mixed $isCancelled
     */
    public function setIsCancelled($isCancelled)
    {
        $this->isCancelled = $isCancelled;
    }

    /**
     * @return mixed
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param mixed $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return mixed
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * @param mixed $purchaseOrder
     */
    public function setPurchaseOrder($purchaseOrder)
    {
        $this->purchaseOrder = $purchaseOrder;
    }
}