<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:50 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_customer")
 **/
class Customer
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="customer_name", type="string", nullable=true)
     */
    private $customerName;

    /**
     * @Column(name="mobile", type="string", nullable=true)
     */
    private $mobile;

    /**
     * @Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @Column(name="nic_number", type="string", nullable=true)
     */
    private $nicNumber;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Entities\Address", mappedBy="customer")
     **/
    private $addresses;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Entities\PurchaseOrder", mappedBy="customer")
     **/
    private $purchaseOrders;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->purchaseOrders = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCustomerName()
    {
        return $this->customerName;
    }

    /**
     * @param mixed $customerName
     */
    public function setCustomerName($customerName)
    {
        $this->customerName = $customerName;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNicNumber()
    {
        return $this->nicNumber;
    }

    /**
     * @param mixed $nicNumber
     */
    public function setNicNumber($nicNumber)
    {
        $this->nicNumber = $nicNumber;
    }

    /**
     * @return ArrayCollection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * @param ArrayCollection $addresses
     */
    public function setAddresses($addresses)
    {
        $this->addresses = $addresses;
    }

    /**
     * @return ArrayCollection
     */
    public function getPurchaseOrders()
    {
        return $this->purchaseOrders;
    }

    /**
     * @param ArrayCollection $purchaseOrders
     */
    public function setPurchaseOrders($purchaseOrders)
    {
        $this->purchaseOrders = $purchaseOrders;
    }
}