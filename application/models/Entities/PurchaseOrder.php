<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:50 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_purchase_order")
 **/
class PurchaseOrder
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @var \DateTime
     *
     * @Column(name="purchase_date", type="datetime")
     */
    private $purchaseDate;

    /**
     * @var ArrayCollection
     *
     * @OneToMany(targetEntity="Entities\BookCode", mappedBy="purchaseOrder")
     **/
    private $bookCodes;

    /**
     * @ManyToOne(targetEntity="Entities\BookShop")
     * @JoinColumn(name="book_shop_id", referencedColumnName="id")
     **/
    private $bookShop;

    /**
     * @ManyToOne(targetEntity="Entities\Customer", inversedBy="purchaseOrders")
     * @JoinColumn(name="customer_id", referencedColumnName="id")
     **/
    private $customer;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->bookCodes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getPurchaseDate()
    {
        return $this->purchaseDate;
    }

    /**
     * @param \DateTime $purchaseDate
     */
    public function setPurchaseDate($purchaseDate)
    {
        $this->purchaseDate = $purchaseDate;
    }

    /**
     * @return ArrayCollection
     */
    public function getBookCodes()
    {
        return $this->bookCodes;
    }

    /**
     * @param ArrayCollection $bookCodes
     */
    public function setBookCodes($bookCodes)
    {
        $this->bookCodes = $bookCodes;
    }

    /**
     * @return mixed
     */
    public function getBookShop()
    {
        return $this->bookShop;
    }

    /**
     * @param mixed $bookShop
     */
    public function setBookShop($bookShop)
    {
        $this->bookShop = $bookShop;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

}