<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:45 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_author")
 **/
class Author
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="author_name", type="string", nullable=true)
     */
    private $authorName;

    /**
     * @Column(name="mobile", type="string", nullable=true)
     */
    private $mobile;

    /**
     * @Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @Column(name="nic_number", type="string", nullable=true)
     */
    private $nicNumber;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param mixed $authorName
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getNicNumber()
    {
        return $this->nicNumber;
    }

    /**
     * @param mixed $nicNumber
     */
    public function setNicNumber($nicNumber)
    {
        $this->nicNumber = $nicNumber;
    }

}