<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 12:46 PM
 */

namespace Entities;

/**
 * @Entity @Table(name="t_publication")
 **/
class Publication
{
    /**
     * @Id @Column(type="integer")
     * @GeneratedValue
     **/
    protected $id;

    /**
     * @Column(name="from_value", type="integer", nullable=true)
     */
    private $fromValue;

    /**
     * @Column(name="to_value", type="integer", nullable=true)
     */
    private $toValue;

    /**
     * @Column(name="subject", type="integer", nullable=true)
     */
    private $subject;

    /**
     * @ManyToOne(targetEntity="\Entities\Book")
     * @JoinColumn(name="book_id", referencedColumnName="id")
     **/
    private $book;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFromValue()
    {
        return $this->fromValue;
    }

    /**
     * @param mixed $fromValue
     */
    public function setFromValue($fromValue)
    {
        $this->fromValue = $fromValue;
    }

    /**
     * @return mixed
     */
    public function getToValue()
    {
        return $this->toValue;
    }

    /**
     * @param mixed $toValue
     */
    public function setToValue($toValue)
    {
        $this->toValue = $toValue;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param mixed $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }
}