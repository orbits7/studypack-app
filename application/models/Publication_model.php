<?php
require_once(APPPATH."models/Entities/Publication.php");
use \Entities\Publication;

/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 8:00 PM
 */
class Publication_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_publication()
    {
        /**
         * @var Publication $publication
         */
        $publication = new Publication();
        $publication->setFromValue(0);
        $publication->setToValue(100);
        $publication->setSubject(1);

        $book = $this->em->find('\Entities\Book', 1);
        $publication->setBook($book);

        try {
            //save to database
            $this->em->persist($publication);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }
}