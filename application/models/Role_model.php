<?php
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 10/05/2017
 * Time: 10:35 PM
 */

require_once(APPPATH."models/Entities/Role.php");
use \Entities\Role;
use \Entities\Privilege;

class Role_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_role()
    {
        /**
         * @var Role $role
         */
        $role = new Role();
        $role->setRoleName('ROLE_SUBSCRIBER');
        $role->setRoleLabel('Subscriber');

        $privilege = $this->em->find('\Entities\Privilege', 1);
        $role->addPrivilege($privilege);

        try {
            //save to database
            $this->em->persist($role);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }

}