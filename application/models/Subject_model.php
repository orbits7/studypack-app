<?php

require_once(APPPATH."models/Entities/Subject.php");
use \Entities\Subject;

/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 2:20 PM
 */
class Subject_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_subject()
    {
        /**
         * @var Subject $subject
         */
        $subject = new Subject();
        $subject->setSubjectName('Physics');

        try {
            //save to database
            $this->em->persist($subject);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }

}