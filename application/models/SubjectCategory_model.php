<?php

require_once(APPPATH."models/Entities/SubjectCategory.php");
use \Entities\SubjectCategory;
/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 1:19 PM
 */
class SubjectCategory_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_category()
    {
        /**
         * @var SubjectCategory $subjectCategory
         */
        $subjectCategory = new SubjectCategory();
        $subjectCategory->setCategoryName('Advanced Level');

        try {
            //save to database
            $this->em->persist($subjectCategory);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }

}