<?php

require_once(APPPATH."models/Entities/Book.php");
use \Entities\Book;

/**
 * Created by PhpStorm.
 * User: isuru
 * Date: 11/05/2017
 * Time: 4:23 PM
 */
class Book_model extends CI_Model
{
    /**
     * @var \Doctrine\ORM\EntityManager $em
     */
    var $em;

    public function __construct() {
        parent::__construct();
        $this->em = $this->doctrine->em;
    }

    /**
     * Add contact messages to database
     * @param array $contact_form
     * @return bool
     */
    function add_book()
    {
        /**
         * @var Book $book
         */
        $book = new Book();
        $book->setTitle('Heat');
        $book->setPrice(1200);

        try {
            //save to database
            $this->em->persist($book);
            $this->em->flush();
        }
        catch(Exception $err){

            die($err->getMessage());
        }
        return true;
    }
}